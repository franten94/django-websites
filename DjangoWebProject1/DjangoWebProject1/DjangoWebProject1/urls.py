"""
Definition of urls for DjangoWebProject1.
"""

from datetime import datetime
from django.conf.urls import patterns, url
from app.forms import BootstrapAuthenticationForm

# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    
    # La manera de agregar una regex a los urls se hace antecediendo la letra r (que significa raw)
    # , la clase o metodo a buscar en nuestro VIEW y un nombre

    # url(r'regularexpression','destinationmethod',name='optionalname'),
    url(r'^artists$', 'app.views.artists',name="artists"),

    # Este regex implica que lo que haga match con los [], sea guardado en una variable name
                    #Y este patron indica que puede contener MAYUS, minus, underscores y espacios(\s)
    #url(r'^artists/(?P<parametro>[A-Za-z_\s]+)$','app.views.artistsdetail',name='artistsdetail' ),

    # Este regex hara match con digitos y sera manejado por el metodo artistsdetail en el view.
    url(r'^artists/(?P<id>[\d]+)$','app.views.artistdetail',name='artistdetail' ),

    url(r'^artists/create$','app.views.createartist',name='createartist'),
    # Examples:
    url(r'^$', 'app.views.home', name='home'),
    url(r'^contact$', 'app.views.contact', name='contact'),
    url(r'^about', 'app.views.about', name='about'),
    url(r'^login/$',
        'django.contrib.auth.views.login',
        {
            'template_name': 'app/login.html',
            'authentication_form': BootstrapAuthenticationForm,
            'extra_context':
            {
                'title':'Log in',
                'year':datetime.now().year,
            }
        },
        name='login'),
    url(r'^logout$',
        'django.contrib.auth.views.logout',
        {
            'next_page': '/',
        },
        name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
