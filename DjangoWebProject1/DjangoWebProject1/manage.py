#!/usr/bin/env python



"""Comandos necesarios para correr el ORM y conectar el MODEL con la DataBAse
python manage.py makemigrations --name initial app
python manage.py makemigrations app
python manage.py showmigrations app
python manage.py sqlmigrate app 0001_initial
python manage.py migrate
"""


"""
Command-line utility for administrative tasks.
"""

import os
import sys

if __name__ == "__main__":
    os.environ.setdefault(
        "DJANGO_SETTINGS_MODULE",
        "DjangoWebProject1.settings"
    )

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
