"""
Definition of views.
"""

from django.shortcuts import render,render_to_response
#Para responder un request hay que hacerlo con HttpResponse (no se importa por defecto)
from django.http import HttpRequest,HttpResponse,HttpResponseRedirect
from django.template import RequestContext
from datetime import datetime
# se import la data (modelo) a la view...
from app.models import *

# Se crea(n) la(s) funcion(es) o metodo(s) especificada por urls.py
"""def artists(request):
    # Ejemplo de un HttpResponse, se construye el html desde aqui
    return HttpResponse('<html><body><h1>Hello Django!</h1></body></html>')
    
def artistsdetail(request,parametro):
    salida = ('<html><head><title>' + parametro + '</title></head>' + '<body><h1>'+ parametro 
    + '</h1></body></html>')
    return HttpResponse(salida) """
 # usando el metodo render se pasa un string a un html
 # -> del modelo a la vista (urls.py->views.py->artists.html)
def artists(request):
        artists = Artist.objects.all()
        return render_to_response('app/artists.html', {'artists':artists})

def artistdetail(request,id):
    # Se traera un objeto a la vez definido por el id matcheado en url
    artist = Artist.objects.get(pk=id)
    # Ese objeto a su vez sera pasado al siguiente html, como variable artist
    return render_to_response('app/artistdetail.html',{'artist':artist})

def createartist(request):
    if request.method == "GET":
        form = ArtistForm()
        return render(request,'app/create.html',{'form':form})
    elif request.method == "POST":
        form = ArtistForm(request.POST)
        form.save()
        return HttpResponseRedirect('/artists')

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        context_instance = RequestContext(request,
        {
            'title':'Home Page',
            'year':datetime.now().year,
        })
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        context_instance = RequestContext(request,
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        })
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        context_instance = RequestContext(request,
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        })
    )
