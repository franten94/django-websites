"""
Definition of models.
"""

from django.db import models
from django import forms

# Create your models here.
class Artist(models.Model):
    # Se crea una propiedad con un nombre igual a models.TipodeData(valores por defecto,etc)
    name = models.CharField("artist",max_length=50)
    year_formed = models.PositiveIntegerField()

class ArtistForm(forms.ModelForm):
    class Meta:
        model= Artist
        fields=['name','year_formed']

class Album(models.Model):
     # El nombre entre comillas es el que se muestra cuando se ejecuta la propiedad
    name = models.CharField("album",max_length=50)
    ## Con esta propiedad se le especifica a Album que es una child de Artist (se relacionan)
    artist = models.ForeignKey(Artist)
