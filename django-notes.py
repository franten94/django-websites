## Making changes to the MODEL (Databases)

# Se importa el archivo models.py de la carpeta py, y a su vez la clase Artist
>>> from app.models import Artist
# Se crea una variable llamando a la clase y pasandole metodos de la clase
>>> newArtist = Artist(name="La Vida Boheme",year_formed=2005)
# Para salvar los cambios
>>> newArtist.save()
# Para actualizar un registro de la DB
>>> newArtist.year_formed=2004
>>> newArtist.save()

### QUERIES to the DB:

#Retrieve all objetcs by using all()
allArtists = Artist.objects.all()
>>> for artist in allArtists:
...     print (artist.name,artist.year_formed)
... 
La Vida Boheme 2004

>>> allArtists[0].id
1
>>> allArtists[0].name
'La Vida Boheme'
>>> allArtists[0].year_formed
2004

# Retrieve single object by its ID using get(ID)
>>> firstArtist = Artist.objects.get(id=1)
>>> print (firstArtist.name,firstArtist.year_formed)
La Vida Boheme 2004

#### OTRO METODO ES EL .filter() trae bastante resultados no com el get (1 solo)

artists = Artist.objects.filter(name__startswith="La V")
# Hara un match y sera sensivito a Mayusculas y Mayusculas
artists = Artist.objects.filter(name__istartswith="La V")

#Lo mismo con get name__exact es un argumento que busca exacamente lo pasado en argumentos
artist = Artist.objects.get(name__exact="La Vida Boheme")
# Si se le antecede con una i sera case-sensitive
artist = Artist.objects.get(name__iexact="La Vida Boheme")

>>> queryArtists = Artist.objects.filter(year_formed=2004)
>>> for artist in queryArtists:
...     print (artist.name)
... 
La Vida Boheme

# multiples queries same line cada punto representa un and y el exclude un exclude xD
queryArtists = Artist.objects.filter(year_formed=2004).filter(country="Canada").exclude(numberOfFans=1)

## Para unir el album con un artista, se importa la clase Album
>>> from app.models import Album
# Luego en la shell se crean nuevos albumes diciendo que el artista sera la variable
# antes guardada newArtist, luego un save para guardarlo en la base de datos.
>>> Album(name="Nuestra",artist=newArtist).save()
>>> Album(name="Sera",artist=newArtist).save()
>>> albums = Album.objects.filter(artist__name="La Vida Boheme")

>>> for album in albums:
...     print (album.name)
... 
Nuestra
Sera

### FORMS (es parte del View (del modelo MVC))####
# Para crear la forma se utiliza el metodo render_to_response que es mejor porque necesita
# el argumento request a diferencia de render.

# El formato de render_to_response es el siguiente
# El primer parametro es el nombre de la plantilla HTML, el segundo las variables que se le pasaran a la plantilla
return render_to_response('template.html',{'variable_name':value})

## es necesario importarlo en views.py
 # se crea el template o el html a mostrar, se agarra a traves de url se manda a la vista para especificar
 # los parametros que se conectaran con la template recien creada.
 # usando el metodo render se pasa un string a un html
 # -> del modelo a la vista (urls.py->views.py->artists.html)

## Ahora tambien se puede actualizar la base de Datos uando formas o templates con botones!
 # Se puede crear una forma a partir de un MODEL ya creado!! 
# Es decir a traves de las clases creadas en models.py se creara una nueva basada en estas clases!

# primero from django import forms
# Se crea el metodo/form basado en Artist,creando una clase llamada Meta,con model=ClaseAnterior(Artist)
class ArtistForm(forms.ModelForm):
    class Meta:
        model= Artist
        fields=['name','year_formed']

# La seguridad esta integrado con Django al agregar crsf_token en los html usados para las forms.
# Si no se usa el token el programa da un error
# en la view es necesario crear el metodo que manejara lo enviado por el url:

def createartist(request):
	# Es el primer metodo cuando el usuario pide la plantilla y este se la entrega para que sea rellenada
    if request.method == "GET":
        form = ArtistForm()
        # me manda pal html pasando la variable form
        return render(request,'app/create.html',{'form':form})
    # Una vez rellenada, se guarda en la base de datos con save(), y se retorna a /artists para que vea los cambios 
    elif request.method == "POST":
        form = ArtistForm(request.POST)
        form.save()
        return HttpResponseRedirect('artists')

# se crea la template create.html dentro de <body>
    <form method="post">
        {% csrf_token %}
        {{form}}
        <button type="submit">Enviar</button>
    </form>

## en urls.py:
url(r'^artists/create$','app.views.createartist',name='createartist'),

## Bootstrap y Layout extend

## Para ahorrarse HTML en c/u de las templates a configurar, se hace un extend del archivo html base en este caso es (layout.html)
## Y se elimina el body y cualquier otra etiqueta referente al html principal y se llama al layout a traves de extend:
{% extends 'app/layout.html' %}
	{{% block content %}}
	<!---- Codigo HTML AQUI--->
	{{% endblock %}}

## En el archivo base encontraremos lo siguiente (app/layout.html):

    <div class="container body-content">
{% block content %}{% endblock %}
    </div>

### Bootstrap
 ## El bootstrap utiliza las divisiones por los div utilizando col-md4
 # col se refiere a columnas y 4 a cuantas columnas se utilizaran 
 # el md es para saber que tipo de Screen se atacara
 # md es medium, lg es large sm es small y xs extrasmall y asi
 # Bootstrap trabaja con 12 columnas y asi se puede dividir la pagina en 3partes 
 # 2 partes 4 partes y asi con cada uno de los col.
 # Ejemplo col-md6 col-md6 ( Se dividira en 2 partes)
 # col-md4 col-md4 col-md4 (Se dividira en 3 partes)
# Crear nuevo <div> y referenciar las columnas con los artistas
<div class="row">
	# Para los nombres se le asigna 3 columnas
	<div class="col-md-3"
		<a href="/artists/{{ artist.id }}"
			{{artist.name }}	
		</a>
	# Para los anos formados 1 columna
	<div class="col-md1">
		{{ artist.year_formed }}
		</div>
</div>
### Conectar con base de datos diferente a sqlite3

# en este ejemplo se colocara una base de datos en MySQL
#1 Crear la base de datos, agarrar el nombre de ella, el usuario, password y host
#2 Ir a settings.py y modificar los parametros dentro de  DATABASES={}
#3 Instalar el modulo de conexion de mysql a traves de pip desde Visual Studio
# Se llama mysqlclient
# Luego de eso hay que correr las migrations con python manage.py migrate
